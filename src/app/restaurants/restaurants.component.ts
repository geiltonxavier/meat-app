import { Component, OnInit } from '@angular/core';
import { Restaurant } from './restaurant/restaurant.model';
import { RestaurantsService } from '../shared/services/restaurants.service';
import { DragulaService } from 'ng2-dragula/ng2-dragula';


@Component({
  selector: 'mt-restaurants',
  templateUrl: './restaurants.component.html'
})
export class RestaurantsComponent implements OnInit {

  restaurants: Restaurant[];
  constructor(private restaurantsService: RestaurantsService, private dragulaService: DragulaService) {
    dragulaService.drag.subscribe((value) => {
      console.log(value);
      // console.log(`drag: ${value[0]}`);
      // this.onDrag(value.slice(1));
    });
    dragulaService.drop.subscribe((value) => {
      console.log(value);
      // console.log(`drop: ${value[0]}`);
      // this.onDrop(value.slice(1));
    });
    dragulaService.over.subscribe((value) => {
      console.log(value);
      // console.log(`over: ${value[0]}`);
      // this.onOver(value.slice(1));
    });
    dragulaService.out.subscribe((value) => {
      console.log(value);
      // console.log(`out: ${value[0]}`);
      // this.onOut(value.slice(1));
    });
  }

  ngOnInit() {
    this.restaurantsService.restaurants()
      .subscribe(restaurants => this.restaurants = restaurants)
  }


  private onDrag(args) {
    let [e, el] = args;
    // do something
  }
  
  private onDrop(args) {
    let [e, el] = args;
    // do something
  }
  
  private onOver(args) {
    let [e, el, container] = args;
    // do something
  }
  
  private onOut(args) {
    let [e, el, container] = args;
    // do something
  }

}
