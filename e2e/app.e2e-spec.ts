import { EatsPage } from './app.po';

describe('Eats App', function() {
  let page: EatsPage;

  beforeEach(() => {
    page = new EatsPage();
  });

  it('Exibe mensagem de Bem vindo ao Eats!', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Bem vindo ao Eats!');
  });

  it('Verifica title do da pagina', () => {
    page.navigateTo();
    expect(page.getTitle()).toEqual('EATS | Angular');
  });

   it('Verifica H1 Sobre', () => {
    page.navigateTo('/about');
    expect(page.getParagraphText()).toEqual('Sobre');
  });

});
