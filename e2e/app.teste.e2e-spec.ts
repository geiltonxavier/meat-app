import { EatsPage } from './app.po';

describe('Eats App', function() {
  let page: EatsPage;

  beforeEach(() => {
    page = new EatsPage();
  });

  it('Exibe mensagem de Bem vindo ao Eats! 2', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Bem vindo ao Eats!');
  });

  it('Verifica title do da pagina 2', () => {
    page.navigateTo();
    expect(page.getTitle()).toEqual('EATS | Angular');
  });

});
