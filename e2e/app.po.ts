import { browser, element, by } from 'protractor';

export class EatsPage {
  navigateTo(navigateTo = '/') {
    browser.waitForAngularEnabled(false);
    return browser.get(navigateTo);
  }

  getParagraphText() {
    return element(by.css('h1')).getText();
  }

  getTitle() {
    return browser.getTitle();
  }

}
